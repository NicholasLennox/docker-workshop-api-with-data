package no.noroff.dockerworkshopapiwithdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerWorkshopApiWithDataApplication {

    public static void main(String[] args) {
        SpringApplication.run(DockerWorkshopApiWithDataApplication.class, args);
    }

}
