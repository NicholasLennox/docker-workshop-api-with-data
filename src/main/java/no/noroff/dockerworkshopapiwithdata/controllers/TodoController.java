package no.noroff.dockerworkshopapiwithdata.controllers;

import no.noroff.dockerworkshopapiwithdata.models.Todo;
import no.noroff.dockerworkshopapiwithdata.repositories.TodoRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("todos")
public class TodoController {
    private TodoRepository _repository;

    public TodoController(TodoRepository repository) {
        _repository = repository;
    }

    @GetMapping
    public ResponseEntity<List<Todo>> getAllTodos() {
        return ResponseEntity.ok(_repository.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Todo> getAllTodos(@PathVariable int id) {
        if(!_repository.existsById(id))
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(_repository.findById(id).get());
    }

    @PostMapping
    public ResponseEntity<Todo> addTodo(@RequestBody Todo todo) {
        Todo response = _repository.save(todo);
        return ResponseEntity.created(null).body(response);
    }
}
